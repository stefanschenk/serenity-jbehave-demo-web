package nl.detesters.web.steps;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import nl.detesters.web.pageobjects.CustomerPage;
import nl.detesters.web.pageobjects.TodoPage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;


/**
 * Created by Stefan on 10/9/2016.
 */
public class CustomerSteps extends ScenarioSteps {
    private final static Logger LOG = LoggerFactory.getLogger(CustomerSteps.class);

    CustomerPage customerPage;

    @Step
    public void opens_the_customer_application() {
        customerPage.open();
    }

}
