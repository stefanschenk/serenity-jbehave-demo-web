package nl.detesters.web.pageobjects;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Stefan on 10/9/2016.
 */
@DefaultUrl("http://localhost/todo/#/")
public class TodoPage extends PageObject {

    public static final String ACTION_ROW = "//div[@class='view' and contains(.,'%s')]";
    public static final String ACTION_ROW_LABEL = "//label[contains(.,'%s')]";
    public static final String COMPLETE_TICKBOX = ".//input[@ng-model='todo.completed']";

    public void addAnActionCalled(String actionName) {
        $("#new-todo").type(actionName)
                .then().sendKeys(Keys.ENTER);
    }

    public List<String> getActions() {
        return findAll(".view").stream()
                .map(WebElementFacade::getText)
                .collect(Collectors.toList());
    }

    public void deleteAllActions() {
        List<String> actions = getActions();

        for (String action : actions) {
            $(String.format(ACTION_ROW, action)).click();
            $(String.format(ACTION_ROW, action)).find(By.className("destroy")).click();
        }
    }
}
