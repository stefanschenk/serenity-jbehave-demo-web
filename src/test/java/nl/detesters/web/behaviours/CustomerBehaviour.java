package nl.detesters.web.behaviours;

import net.thucydides.core.annotations.Steps;
import nl.detesters.web.steps.CustomerSteps;
import nl.detesters.web.steps.TodoSteps;
import org.jbehave.core.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by Stefan on 10/9/2016.
 */
public class CustomerBehaviour {
    private final static Logger LOG = LoggerFactory.getLogger(CustomerBehaviour.class);

    @Steps
    CustomerSteps customerSteps;

    @Given("I need to work with customers")
    public void givenTheTodoApplicationIsOpened() {
        customerSteps.opens_the_customer_application();
    }


}
