Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Open the Todo application and reset it (remove all todos)
Meta:

Given I need to open the Todo application
When I delete all todo actions